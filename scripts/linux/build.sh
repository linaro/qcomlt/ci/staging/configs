#!/bin/sh

# Build the linux kernel checkouted at CWD
#
# Input:
# - CWD: checkouted kernel git tree
# - $CI_PROJECT_DIR: should be CWD
# - $CI_JOB_STAGE: CLO CI Job Stage name
# - $CI_JOB_NAME: CLO CI Job name
# - $ARCH: kernel architecture to build (ARM or ARM64)
#
# Output:
# - out/kernel/Image*
# - out/kernel/System.map
# - out/kernel/vmlinux
# - out/kernel/kernel.config
# - out/kernel/modules.tar.xz
# - out/kernel/dtbs.tar.xz
# - out/kernel/parameters.env

set -ex

# Build variables
export KERNEL_COMMIT="$(git rev-parse HEAD)"
export KERNEL_VERSION="$(make kernelversion)"
export KERNEL_DESCRIBE="$(git describe --always)"
export KERNEL_BUILD_DIR=$CI_PROJECT_DIR/build
export KERNEL_OUT_DIR=$CI_PROJECT_DIR/out/kernel

# SRCVERSION is the main kernel version, e.g. <version>.<patchlevel>.0.
SRCVERSION=$(echo ${KERNEL_VERSION} | sed 's/\(.*\)\..*/\1.0/')

mkdir -p $KERNEL_BUILD_DIR
mkdir -p $KERNEL_OUT_DIR

# Make config
make O=$KERNEL_BUILD_DIR ${KERNEL_CONFIG}

# Build kernel
make O=$KERNEL_BUILD_DIR KERNELRELEASE=${SRCVERSION}-qcomlt-${ARCH} -s -j$(nproc)

# Install modules
make O=$KERNEL_BUILD_DIR KERNELRELEASE=${SRCVERSION}-qcomlt-${ARCH} -s -j$(nproc) INSTALL_MOD_STRIP=1 INSTALL_MOD_PATH=$KERNEL_OUT_DIR/modules modules_install

# Copy modules into modules.tar.xz
(cd $KERNEL_OUT_DIR/modules && tar cJf ../modules.tar.xz .)
rm -fr $KERNEL_OUT_DIR/modules

# Install dtbs
make O=$KERNEL_BUILD_DIR -s INSTALL_DTBS_PATH=$KERNEL_OUT_DIR/dtbs dtbs_install

# Copy dtbs into dtbs.tar.xz
(cd $KERNEL_OUT_DIR/dtbs && tar cJf ../dtbs.tar.xz .)
rm -fr $KERNEL_OUT_DIR/dtbs

# Copy config
cp $KERNEL_BUILD_DIR/.config $KERNEL_OUT_DIR/kernel.config

# Copy System.map, vmlinux & Image
cp $KERNEL_BUILD_DIR/{System.map,vmlinux} $KERNEL_OUT_DIR/
cp $KERNEL_BUILD_DIR/arch/$ARCH/boot/Image* $KERNEL_OUT_DIR/

# Clean up
rm -fr $KERNEL_BUILD_DIR

# Write kernel_parameters file containing sourceable variables to use build artifacts
cat > $KERNEL_OUT_DIR/parameters.env << EOF
KERNEL_IMAGE_PATH="out/kernel/Image"
KERNEL_MODULES_PATH="out/kernel/modules.tar.xz"
KERNEL_DT_PATH="out/kernel/dtbs.tar.xz"
KERNEL_ARTIFACTS_URL="${CI_SERVER_URL}/api/v4/projects/${CI_PROJECT_ID}/jobs/${CI_JOB_ID}/artifacts/"
KERNEL_REPO_URL="${CI_PROJECT_URL}"
KERNEL_COMMIT="${KERNEL_COMMIT}"
KERNEL_BRANCH="${CI_COMMIT_REF_NAME}"
KERNEL_CONFIG="${KERNEL_CONFIG}"
KERNEL_VERSION="${KERNEL_VERSION}"
KERNEL_DESCRIBE="${KERNEL_DESCRIBE}"
KERNEL_TOOLCHAIN="${KERNEL_TOOLCHAIN}"
EOF

echo "Build success, parameters:"
cat $KERNEL_OUT_DIR/parameters.env
