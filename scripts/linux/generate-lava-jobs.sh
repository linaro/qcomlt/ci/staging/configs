#!/bin/sh

# Generate lava jobs
#
# Input:
# - $CI_PROJECT_DIR: base dir of CI build
# - $CI_PROJECT_DIR/out/kernel/parameters.env
# - $CI_PROJECT_DIR/out/$MACHINE/bootimg.env
# - $MACHINE
#
# Output:
# - $CI_PROJECT_DIR/out/$MACHINE/lava/*.yaml

set -ex

source out/kernel/parameters.env
source out/$MACHINE/bootimg.env

export LAVA_JOB_PRIORITY="high"

# Clone linaro's lava-test-plans
if [[ ! -d $CI_PROJECT_DIR/lava-test-plans ]]; then
    # clone lava-test-plans repository
    git clone --depth 1 $LAVA_TEST_PLANS_GIT_REPO -b $LAVA_TEST_PLANS_GIT_BRANCH lava-test-plans
fi

# lava-test-plans setup
pushd lava-test-plans
git rev-parse HEAD
# TODO: workaround while project requirements file is created again
if [ ! -f requirements.txt ]; then
        cat << EOF > requirements.txt
requests
ruamel.yaml
Jinja2
docker
configobj
EOF
fi
pip3 install -r requirements.txt
rm requirements.txt
popd

# Create variables file to use with lava-test-plans submit_for_testing.py
create_testing_variables_file () {
    mkdir -p $(dirname $1)
    BOOT_URL="${2}${3}"
    LXC_BOOT_FILE="$(basename ${3} .gz)"
    BOOT_URL_COMP="gz"

        cat << EOF > $1
"LAVA_JOB_PRIORITY": "${LAVA_JOB_PRIORITY}"

"PROJECT": "projects/lt-qcom/"
"PROJECT_NAME": "lt-qcom"
"OS_INFO": "kernel"

"BUILD_URL": "${CI_PIPELINE_URL}"
"BUILD_NUMBER": "${CI_PIPELINE_ID}"
"KERNEL_REPO": "${KERNEL_REPO_URL}"
"KERNEL_BRANCH": "${KERNEL_BRANCH}"
"KERNEL_COMMIT": "${KERNEL_COMMIT}"
"KERNEL_DESCRIBE": "${KERNEL_DESCRIBE}"
"KERNEL_CONFIG": "${KERNEL_CONFIG}"
"TOOLCHAIN": "${KERNEL_TOOLCHAIN}"

"DEPLOY_OS": "oe"
"BOOT_URL": "${BOOT_URL}"
"BOOT_URL_COMP": "${BOOT_URL_COMP}"
"LXC_BOOT_FILE": "${LXC_BOOT_FILE}"

"WLAN_DEVICE": "${WLAN_DEVICE}"
"ETH_DEVICE": "${ETH_DEVICE}"
"HCI_DEVICE": "${HCI_DEVICE}"
EOF

    cat $1
}

lava_test_gen () {
    test_name=${1}

    test_plans_file="${CI_PROJECT_DIR}/out/${MACHINE}/lava_test_plans_${test_name}.yaml"

    create_testing_variables_file ${test_plans_file} "${2}" "${3}"

    pushd lava-test-plans

    # Only generate yaml here, send them later to Squad
    python3 -m lava_test_plans \
        --device-type ${LAVA_DEVICE_TYPE} \
        --build-number ${KERNEL_DESCRIBE} \
        --testplan-device-path lava_test_plans/projects/lt-qcom/devices \
        --test-case testcases/${test_name}.yaml \
        --variables ${test_plans_file}\
        --dry-run
    
    cp tmp/${LAVA_DEVICE_TYPE}/${test_name}.yaml ${CI_PROJECT_DIR}/out/${MACHINE}/lava/${test_name}.yaml

    # Cleanup
    rm ${test_plans_file} tmp/${LAVA_DEVICE_TYPE}/${test_name}.yaml

    popd
}

mkdir -p ${CI_PROJECT_DIR}/out/${MACHINE}/lava/

# TODO get board test parameters from config file
WLAN_DEVICE="wlan0"
ETH_DEVICE="eth0"
HCI_DEVICE="hci0"
case "${MACHINE}" in
    apq8016-sbc)
        LAVA_DEVICE_TYPE="dragonboard-410c"
        ;;
    
    apq8096-db820c)
        LAVA_DEVICE_TYPE="dragonboard-820c"
        ;;
    
    sdm845-db845c)
        LAVA_DEVICE_TYPE="dragonboard-845c"
        ;;
    
    qcs404-evb-4000)
        LAVA_DEVICE_TYPE="qcs404-evb-4k"
        ;;
    
    sdm845-mtp|sm8150-mtp|sm8250-mtp|sm8350-mtp)
        LAVA_DEVICE_TYPE="${MACHINE}"
        ;;

    *)
    echo "No LAVA test for ${MACHINE}"

    exit 0
    ;;
esac

# TODO use different test suite depending on the OE ramdisk image manifest
lava_test_gen "oe-rpb-initramfs-test" ${BOOTIMG_ARTIFACTS_URL} ${BOOT_RAMDISK_FILE}
