#!/bin/sh

# Customizes a ramdisk with the kernel modules used for LAVA tests
#
# Input:
# - $CI_PROJECT_DIR: base dir of CI build
# - $CI_PROJECT_DIR/out/kernel/parameters.env: should be a sourceable file containing the kernel build variables
# - $CI_PROJECT_DIR/out/ramdisk.cpio.gz: ramdisk with modules
# - $MACHINE
#
# Output:
# - $CI_PROJECT_DIR/out/$MACHINE/boot.img.gz
# - $CI_PROJECT_DIR/out/$MACHINE/bootimg.env

set -ex

wget_error() {
    wget --quiet --timeout=60 --progress=dot -c $1
    retcode=$?
    if [ ${retcode} -ne 0 ]; then
        exit ${retcode}
    fi
}

# Get kernel parameters (KERNEL_IMAGE_PATH, KERNEL_DT_PATH)
source ${CI_PROJECT_DIR}/out/kernel/parameters.env
source ${CI_PROJECT_DIR}/out/ramdisk.env

export GZ=pigz

# Clone Android mkbootimg
[ -d $CI_PROJECT_DIR/mkbootimg ] || git clone --depth 1 https://android.googlesource.com/platform/system/tools/mkbootimg/
chmod +x mkbootimg/mkbootimg.py

wget_error ${RAMDISK_ARTIFACTS_URL}${RAMDISK_FILE}

BOOT_RAMDISK_OUT_FILE="boot.img.gz"

BOOTIMG_BASE=0x80000000
RAMDISK_OFFSET=0x4000000
SERIAL_CONSOLE=ttyMSM0
KERNEL_DT="dtbs/qcom/${MACHINE}.dtb"

mkdir -p ${CI_PROJECT_DIR}/out/${MACHINE}/

# Unpack dtbs
mkdir dtbs
tar -xJf ${KERNEL_DT_PATH} -C dtbs

# Compress Image file
${GZ} -c ${KERNEL_IMAGE_PATH} > ${CI_PROJECT_DIR}/Image.gz
kernel_file="${CI_PROJECT_DIR}/Image.gz"

# TODO move that to a config file
case "${MACHINE}" in
    apq8096-db820c|sdm845-db845c)
        BOOTIMG_PAGESIZE=4096
        ;;
    *)
        BOOTIMG_PAGESIZE=2048
        ;;
esac

# TODO move that to a config file
case "${MACHINE}" in
    sdm845-db845c)
        KERNEL_CMDLINE_APPEND="clk_ignore_unused pd_ignore_unused"
        ;;
    *)
        KERNEL_CMDLINE_APPEND=""
        ;;
esac

# TODO Header version and method to pass DT should be in a config file

# Append DT to kernel Image
cat ${kernel_file} ${KERNEL_DT} > ${kernel_file}_dt

# Create boot image with ramdisk-only
mkbootimg/mkbootimg.py \
        --header_version 1 \
        --kernel ${kernel_file}_dt \
        --ramdisk ${RAMDISK_FILE} \
        --output boot.img \
        --pagesize ${BOOTIMG_PAGESIZE} \
        --base ${BOOTIMG_BASE} \
        --ramdisk_offset ${RAMDISK_OFFSET} \
        --cmdline "root=/dev/ram0 init=/sbin/init rw console=tty0 console=${SERIAL_CONSOLE},115200n8 earlycon debug net.ifnames=0 ${KERNEL_CMDLINE_APPEND}"

${GZ} -c boot.img > ${BOOT_RAMDISK_OUT_FILE}

curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${BOOT_RAMDISK_OUT_FILE} "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/bootimg-${CI_PIPELINE_ID}/${MACHINE}/boot.img.gz"

# Cleanup
rm boot.img ${BOOT_RAMDISK_OUT_FILE}
rm -fr dtbs
rm ${kernel_file} ${kernel_file}_dt

# Write variable to env file with filename and public URL
cat > out/${MACHINE}/bootimg.env << EOF
BOOT_RAMDISK_FILE="boot.img.gz"
BOOTIMG_ARTIFACTS_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/bootimg-${CI_PIPELINE_ID}/${MACHINE}/"
EOF

cat out/${MACHINE}/bootimg.env
